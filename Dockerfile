FROM node:11
RUN mkdir /usr/src/app
WORKDIR /usr/src/app
RUN npm install
RUN npm install -g concurrently
COPY . .
